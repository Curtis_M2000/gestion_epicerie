package exo1;

public class Viande {
    private IViande maViande;
    
    public Viande(){
        this.maViande = new ViandeDB();
    }
    
    public void setSelectionType(IViande selection){
        this.maViande = selection;
    }
    
    public void InsertViande(String a, float b, String c){
        this.maViande.insert(a, b, c);
    }
    
    public void UpdateViande(String a, float b){
        this.maViande.update(a, b);
    }
    
    public void DeleteViande(String a){
        this.maViande.delete(a);
    }
    
    public String SelectViande(String a){
        return this.maViande.select(a);
    }
    
    public String TotalViande(){
        return this.maViande.total();
    }
}
