package exo1;

public class Legume {
    
    private SaveLegume monLegume;
    
    public Legume(){
        this.monLegume = new LegumeDB();
    }
    
    public void setSelectionType(SaveLegume selection){
        this.monLegume = selection;
    }
    
    public void InsertLegume(String a, float b, String c){
        this.monLegume.insert(a, b, c);
    }
    
    public void UpdateLegume(String a, float b){
        this.monLegume.update(a, b);
    }
    
    public void DeleteLegume(String a){
        this.monLegume.delete(a);
    }
    
    public String SelectLegume(String a){
        return this.monLegume.select(a);
    }
    
    public String TotalLegume(){
        return this.monLegume.total();
    }
}