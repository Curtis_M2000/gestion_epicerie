package exo1;

public interface SaveLegume {
    public void insert(String a, float b, String c);
    public void update(String a, float b);
    public void delete(String a);
    public String select(String a);
    public String total();
}

