package exo1;
import java.sql.DriverManager;
import java.sql.Connection;import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

public class ViandeDB implements IViande {
    @Override 
    public void insert(String nom, float prix, String type){
        String insertion = "INSERT INTO viande (nom, prix, type) VALUES(?, ?, ?)";
        String url = "jdbc:mysql://localhost:3306/produits";
        String user = "root";
        String pw = "";
        
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            PreparedStatement stmtInsert = conn.prepareStatement(insertion);
            
            stmtInsert.setString(1, nom);
            stmtInsert.setFloat(2, prix);
            stmtInsert.setString(3, type);
            
            stmtInsert.execute();
            conn.close();
        } 
        catch (SQLException ex) {
            Logger.getLogger(Exo1.class.getName()).log(Level.SEVERE, null, ex);

        }
        finally{}
    }
    
    @Override
    public void update(String nom, float prix){
        String update = "UPDATE viande SET prix = ? WHERE nom = ?";
        String url = "jdbc:mysql://localhost:3306/produits";
        String user = "root";
        String pw = "";
        
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            PreparedStatement stmtUpdate = conn.prepareStatement(update);
            
            stmtUpdate.setFloat(1, prix);
            stmtUpdate.setString(2, nom);          
            
            stmtUpdate.execute();
            conn.close();
        } 
        catch (SQLException ex) {
            Logger.getLogger(Exo1.class.getName()).log(Level.SEVERE, null, ex);

        }
        finally{}
    }
    
    @Override
    public void delete(String nom){
        String delete = "DELETE FROM viande WHERE nom = ?";
        String url = "jdbc:mysql://localhost:3306/produits";
        String user = "root";
        String pw = "";
        
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            PreparedStatement stmtDelete = conn.prepareStatement(delete);
            
            stmtDelete.setString(1, nom);          
            
            stmtDelete.execute();
            conn.close();
        } 
        catch (SQLException ex) {
            Logger.getLogger(Exo1.class.getName()).log(Level.SEVERE, null, ex);

        }
        finally{}
    }
    
    @Override
    public String select(String nom){
        String select = "SELECT * FROM viande WHERE nom = '" + nom + "'";
        String url = "jdbc:mysql://localhost:3306/produits";
        String user = "root";
        String pw = "";
        String monText = "";
        
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            PreparedStatement stmtAffiche = conn.prepareStatement(select);         
            ResultSet result = stmtAffiche.executeQuery();
            
            
            while(result.next()){
                double tax = result.getFloat("prix") * 0.2;
                double total = result.getFloat("prix") + tax;
                monText += "Id = " + result.getInt("id");
                monText += "\nNom = " + result.getString("nom");
                monText += "\nType = " + result.getString("type");
                monText += "\nPrix = " + result.getFloat("prix");
                monText += "\nTax = " +Double.toString(tax);
                monText += "\nTotal = " +Double.toString(total);
            }
            
            conn.close();
        } 
        catch (SQLException ex) {
            Logger.getLogger(Exo1.class.getName()).log(Level.SEVERE, null, ex);

        }
        finally{}
        return monText;
    }
    
    @Override
    public String total(){
        String select = "SELECT * FROM viande";
        String url = "jdbc:mysql://localhost:3306/produits";
        String user = "root";
        String pw = "";
        String monText = "";
        int i=0;
        Float prixTotal = 0f;
        Double tax = 0.0;
        Double total = 0.0;
        
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            PreparedStatement stmtAffiche = conn.prepareStatement(select);         
            ResultSet result = stmtAffiche.executeQuery();
            
            
            while(result.next()){
                float prixViande = result.getFloat("prix");
                prixTotal += prixViande;
                i++;
            }
            
            tax = prixTotal * 0.2;
            total = tax + prixTotal;
            
            monText += "Nombre de Viande = " + Integer.toString(i);
            monText += "\nPrix Total = " + Float.toString(prixTotal);
            monText += "\nTax Total = " + Double.toString(tax);
            monText += "\nPrix Total + Tax = " + Double.toString(total);
            
            conn.close();
        } 
        catch (SQLException ex) {
            Logger.getLogger(Exo1.class.getName()).log(Level.SEVERE, null, ex);

        }
        finally{}
        return monText;
    }
}
